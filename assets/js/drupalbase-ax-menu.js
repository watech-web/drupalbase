(function ($, Drupal, drupalSettings) {
	'use strict';
	Drupal.behaviors.ax_menu = {
		attach: function (context, settings) {
			$('.we-mega-menu-li').focusin(function(){
				$(this).addClass('tabbed-open');
				$(this).children().first('a.we-mega-menu-li').attr({'aria-haspopup':'true','aria-expanded':'true'});
				$('li.we-mega-menu-li').not(this).each(function(){
					$(this).removeClass('tabbed-open');
					$(this).children().first('a.we-mega-menu-li').attr({'aria-haspopup':'false','aria-expanded':'false'});
				});
			});
			$('.navbar-we-mega-menu').focusout( function() {
				$('li.we-mega-menu-li').removeClass('tabbed-open');
				$('a.we-mega-menu-li').attr({'aria-haspopup':'false','aria-expanded':'false'});
			});			
		}
	}
})(jQuery, Drupal, drupalSettings);